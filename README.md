# pywitness
[![pipeline status](https://gitlab.com/sheddow/pywitness/badges/master/pipeline.svg)](https://gitlab.com/sheddow/pywitness/commits/master) [![coverage report](https://gitlab.com/sheddow/pywitness/badges/master/coverage.svg)](https://gitlab.com/sheddow/pywitness/commits/master)

Witness the `pywitness`. Makes your recon process ludicrously speedy by screenshotting websites for you. Requires python 3.7.