import os.path
import subprocess
import tempfile

import pytest

from pywitness.browser import Browser, Screenshot, ScreenshotError
from pywitness.browserpool import BrowserPool
from pywitness.report import Report

# pylint: disable=redefined-outer-name
POOL_SIZE = 4


def count_browsers():
    p = subprocess.run(["pgrep", "geckodriver", "--count"], stdout=subprocess.PIPE)
    return int(p.stdout)


@pytest.fixture()
def browser():
    browser_count = count_browsers()
    with Browser() as browser:
        yield browser
    assert count_browsers() == browser_count


@pytest.fixture()
def browser_pool():
    browser_count = count_browsers()
    with BrowserPool(POOL_SIZE) as pool:
        yield pool
    assert count_browsers() == browser_count


def test_browser(browser):
    scr = browser.take_screenshot("example.com")
    assert isinstance(scr, Screenshot)
    scr = browser.take_screenshot("doesntexist.example.com")
    assert isinstance(scr, ScreenshotError)


def test_browserpool(browser_pool):
    domains = ["google.com", "twitter.com"]
    screenshots = list(browser_pool.take_screenshots(domains))
    assert len(screenshots) == 2
    assert all(isinstance(s, Screenshot) for s in screenshots)


def test_report(browser):
    with tempfile.TemporaryDirectory() as tmpdir:
        report = Report(tmpdir)
        scr = browser.take_screenshot("example.com")
        report.add_screenshot(scr)
        scr2 = browser.take_screenshot("doesntexist.example.com")
        report.add_screenshot(scr2)
        report.write()

        assert os.path.isfile(tmpdir + "/report.html")
