import setuptools


with open('README.md') as f:
    readme = f.read()

setuptools.setup(
    name='pywitness',
    version='0.1',
    description='The all-seeing py',
    long_description=readme,
    long_description_content_type='text/markdown',
    url='https://gitlab.com/sheddow/pywitness',
    author='Sigurd Kolltveit',
    author_email='sigurd.kolltveit@gmx.com',
    license='MIT',
    packages=['pywitness'],
    package_data={'pywitness': ['templates/*.html']},
    install_requires=[
        'selenium>=3.14.0',
        'tqdm>=4.24.0'
    ],
    python_requires='>=3.5',
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    entry_points={
        'console_scripts': [
            'pywitness = pywitness.main:main',
        ],
    },
    zip_safe=False)
