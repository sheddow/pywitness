import json
import os
import string
import uuid
from typing import List, Dict
from collections import defaultdict

from .browser import Screenshot, ScreenshotError, ScreenshotType


class Entry:
    def __init__(self) -> None:
        self.image_path: str = ''
        self.image_data: bytes = b''
        self.domains: List[str] = []

    def save_image(self, path: str) -> None:
        with open(path, 'wb') as f:
            f.write(self.image_data)


class Report:
    def __init__(self, out_dir: str) -> None:
        self.entries: Dict[bytes, Entry] = defaultdict(Entry)
        self.errors: List[ScreenshotError] = []
        self.out_dir = out_dir
        self.images_dir = os.path.join(out_dir, "images")

    def add_screenshot(self, screenshot: ScreenshotType) -> None:
        if isinstance(screenshot, Screenshot):
            entry = self.entries[screenshot.image_data]
            entry.image_data = screenshot.image_data
            entry.domains.append(screenshot.domain)
        elif isinstance(screenshot, ScreenshotError):
            self.errors.append(screenshot)

    def write(self) -> None:
        os.makedirs(self.images_dir, exist_ok=True)

        for entry in self.entries.values():
            path = os.path.join(self.images_dir, str(uuid.uuid4())) + ".png"
            entry.save_image(path)
            entry.image_path = os.path.relpath(path, start=self.out_dir)

        current_dir = os.path.dirname(os.path.realpath(__file__))
        with open(current_dir + '/templates/report.html') as f:
            template = string.Template(f.read())

        screenshots = [
            {'domains': e.domains, 'image': e.image_path} for e in self.entries.values()
        ]

        errors = [
            {'domain': err.domain, 'msg': err.error_msg} for err in self.errors
        ]

        report = template.substitute(
            screenshots=json.dumps(screenshots),
            errors=json.dumps(errors))

        with open(os.path.join(self.out_dir, "report.html"), 'w') as f:
            f.write(report)
