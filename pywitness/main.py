import argparse
from typing import Sequence

from tqdm import tqdm

from .browserpool import BrowserPool
from .report import Report
from .browser import ScreenshotType


def main() -> None:
    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--processes", type=int, default=10)
    parser.add_argument("-d", "--domains", type=argparse.FileType('r'), required=True)
    parser.add_argument("-o", "--out", default="out")
    args = parser.parse_args()

    domains = [d.strip() for d in args.domains.readlines()]

    report = Report(args.out)

    with BrowserPool(args.processes) as pool:
        screenshot_iter = pool.take_screenshots(domains)
        progress_bar: Sequence[ScreenshotType] = tqdm(screenshot_iter, total=len(domains))
        for screenshot in progress_bar:
            report.add_screenshot(screenshot)

    report.write()
