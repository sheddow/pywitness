from __future__ import annotations
import random
import re
import time
import urllib.parse
from typing import Union, Any

from selenium.webdriver import Firefox, FirefoxProfile
from selenium.webdriver.firefox.options import Options

TIMEOUT = 15


class Browser:
    def __init__(self) -> None:
        options = Options()
        options.add_argument('-headless')
        profile = FirefoxProfile()
        profile.accept_untrusted_certs = True
        self._browser = Firefox(executable_path='geckodriver',
                                options=options,
                                firefox_profile=profile)
        self._browser.set_page_load_timeout(TIMEOUT)

    def __enter__(self) -> Browser:
        return self

    def __exit__(self, exc_type: Any, exc_value: Any, traceback: Any) -> None:
        self._browser.quit()

    def take_screenshot(self, domain: str) -> ScreenshotType:
        url = "https://" + domain
        try:
            self._browser.get(url)
        except Exception as e:  # pylint: disable=broad-except
            return ScreenshotError(domain, e)
        time.sleep(random.uniform(3, 6))  # Wait for javascript to load
        return Screenshot(domain, self._browser.get_screenshot_as_png())


class ScreenshotError:
    def __init__(self, domain: str, exc: Exception) -> None:
        self.domain = domain
        self.error_msg = str(exc)
        m = re.search(r"about:neterror.*$", str(exc))
        if m:
            loc = m.group()
            query_params = urllib.parse.parse_qs(urllib.parse.urlparse(loc).query)
            if 'd' in query_params and 'e' in query_params:
                self.error_msg = "{} ({})".format(
                    query_params['d'][0],
                    query_params['e'][0])


class Screenshot:
    def __init__(self, domain: str, data: bytes) -> None:
        self.domain = domain
        self.image_data = data


ScreenshotType = Union[Screenshot, ScreenshotError]
