from __future__ import annotations
from multiprocessing import Process, Queue
from typing import Iterator, Sequence, List, Any, Optional  # pylint: disable=unused-import

from .browser import Browser, ScreenshotType


class BrowserPool:
    def __init__(self, processes: int) -> None:
        self.processes = processes
        self._inqueue: 'Queue[Optional[str]]' = Queue()
        self._outqueue: 'Queue[ScreenshotType]' = Queue()
        self._pool: List[Process] = []

    def take_screenshots(self, domains: Sequence[str]) -> Iterator[ScreenshotType]:
        self._pool = []
        for _ in range(self.processes):
            p = Process(target=browser_process, args=(self._inqueue, self._outqueue))
            self._pool.append(p)
            p.start()

        for d in domains:
            self._inqueue.put(d)

        for _ in range(len(self._pool)):
            self._inqueue.put(None)

        for _ in range(len(domains)):
            yield self._outqueue.get()

    def close(self) -> None:
        for process in self._pool:
            process.join()

    def __enter__(self) -> BrowserPool:
        return self

    def __exit__(self, exc_type: Any, exc_value: Any, traceback: Any) -> None:
        self.close()


def browser_process(inqueue: 'Queue[Optional[str]]', outqueue: 'Queue[ScreenshotType]') -> None:
    with Browser() as browser:
        while True:
            domain = inqueue.get()
            if domain is None:
                break
            screenshot = browser.take_screenshot(domain)
            outqueue.put(screenshot)
